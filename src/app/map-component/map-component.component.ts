import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { HereService } from '../here-component/here.service';



declare var H: any;


@Component({
    selector: 'app-map-component',
    templateUrl: './map-component.component.html',
    styleUrls: ['./map-component.component.css']
})
export class MapComponentComponent implements OnInit {

    
    private ui: any;
    private search: any;

    @ViewChild("map")
    public mapElement: ElementRef;

    @Input()
    public appId: any;

    @Input()
    public appCode: any;

    @Input()
    public lat: any;

    @Input()
    public lng: any;

    @Input()
    public width: any;

    @Input()
    public height: any;

    @Input()
    public coords = null;

    

    private platform: any;
    private map: any;
    private showMap = true



    public constructor(
        // private hereService:HereService
        private data: HereService
    ) {
    }

    public ngOnInit() {
        this.platform = new H.service.Platform({
            "app_id": this.appId,
            "app_code": this.appCode
        });

        this.search = new H.places.Search(this.platform.getPlacesService());
    }

    public ngOnChanges() {
        console.log("coords",this.coords)
        
        setTimeout(() => {
            this.updateMap(this.map)
        }, 500)  

    }



    public ngAfterViewInit() {
        
        this.renderMap()
    }

    public renderMap() {
        let defaultLayers = this.platform.createDefaultLayers();
        this.map = new H.Map(
            this.mapElement.nativeElement,
            defaultLayers.normal.map,
            {
                zoom: 10,
                center: { 
                    lat: this.lat,
                    lng: this.lng
                }
            }
        );
        let behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map , this.coords));
        this.ui = H.ui.UI.createDefault(this.map, defaultLayers);
        
    }

    public updateMap(map) {
            map.setCenter({
                lat: this.coords && this.coords.latitude ? this.coords.latitude : 46.25449, 
                lng: this.coords && this.coords.longitude ? this.coords.longitude : 20.14857     
            });
            map.setZoom(12);
    }


}



import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HereComponentComponent } from './here-component.component';

describe('HereComponentComponent', () => {
  let component: HereComponentComponent;
  let fixture: ComponentFixture<HereComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HereComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HereComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

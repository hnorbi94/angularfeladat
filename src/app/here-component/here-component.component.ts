import { Component, EventEmitter, Output } from '@angular/core';
import { HereService } from "./here.service";
import {MatButtonModule} from '@angular/material/button';

@Component({
    selector: 'app-here-component',
    templateUrl: './here-component.component.html',
    styleUrls: ['./here-component.component.css']
})
export class HereComponentComponent {
    @Output() event = new EventEmitter()
    
    public query: string;
    public position: string;
    public locations: Array<any>;

    public constructor(
        private here: HereService
    ) {
        this.query = "";
        this.position = "";
    }

    public getAddress() {
      if(this.query != "") {
        this.here.getAddress(this.query).then(result => {
            this.locations = <Array<any>>result;
            this.event.emit({type: 'get-back-location', payload: {
                latitude: result[0].Location.DisplayPosition.Latitude || null,
                longitude: result[0].Location.DisplayPosition.Longitude || null
            }})
            console.log(this.locations);
        }, error => {
            console.error(error);
        });
    }
     }

    public getAddressFromLatLng() {
      if(this.position != "") {
        this.here.getAddressFromLatLng(this.position).then(result => {
            this.locations = <Array<any>>result;
            this.event.emit({type: 'get-lat-lng', payload: {
                latitude: result[0].Location.DisplayPosition.Latitude || null,
                longitude: result[0].Location.DisplayPosition.Longitude || null
            }})

            console.log(this.locations);
        }, error => {
            console.error(error);
        });
     }
    }
    
    }
  
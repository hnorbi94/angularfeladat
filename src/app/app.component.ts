import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public query: string;
  public coords = null;

  public constructor() {
    this.query = "";
  }

  public ngOnInit() {

  }


  public onChildEvent(event) {
    console.log("on-child-event", event)
    switch (event.type) {
     
      case 'get-back-location':
        this.coords = event.payload
        break;

      case 'get-lat-lng':
        this.coords = event.payload
        break;
    }
  }


}


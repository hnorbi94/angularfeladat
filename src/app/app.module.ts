import { HereService } from './here-component/here.service';
import { WeaterComponentComponent } from './weater-component/weater-component.component';
import { FormsModule } from '@angular/forms';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HereComponentComponent } from './here-component/here-component.component';
import { MapComponentComponent } from './map-component/map-component.component';
import { HttpModule } from '@angular/http'  
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';




@NgModule({
  declarations: [
    AppComponent,
    HereComponentComponent,
    MapComponentComponent,
    WeaterComponentComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    HttpModule,
    CommonModule,
    MatButtonModule,
    MatCheckboxModule,
    ],
  exports: [MatButtonModule, MatCheckboxModule],
  providers: [HereService],
  bootstrap: [AppComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
